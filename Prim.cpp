#include <cstring>
#include <iostream>
#include <fstream>
#include "Prim.h"

#define INF 9999999

using namespace std;
/* Constructor por defecto */
Prim::Prim() {
}
/* Metodo que toma el número de nodos, 
la matriz de distancias y el vector de nodos y aplica el algoritmo
de Prim */
void Prim::aplicar_prim(int V, int **G, string *nodos) {
  /* Número de vertices, parte de cero a n */
  int no_edge; 
  /* Peso o costo de las aristas (u, v) */
  int *pesos;
  /* Nodo de que parte a definir la distancia con el siguiente, entre ambos */
  int *nodo_ini;
  /* Nodos siguiente al inicial, ambos comforma una arista */
  int *nodo_sig;
  pesos = new int[V];
  nodo_ini = new int[V];
  nodo_sig = new int[V];
  /* Crea una arreglo que guarda el vertice seleccionado */
  /* Selección es falsa por defecto a menor que se cumpla el requisito */
  int selected[V];

  /* Se deja falso por defecto */
  memset(selected, false, sizeof(selected));

  /* Se parte el contador de vertice en 0 */
  no_edge = 0;

  /* El número de vertices en el algortimo será siempre V - 1
  donde V es el número de nodos */

  /* Se selecciona el primer vertice y se hace verdadero */
  selected[0] = true;
  /* Número de filas */
  int x;  
  /* Número de columna */
  int y;  

  /* Salidas, titulos de los datos para el usuario */
  cout << "(u, v)"
     << "  : "
     << "Costo "
     << ":  L = {}";
  cout << endl;
  while (no_edge < V - 1) {
    /* Por cada vertice en el conjunto s, se encuentra 
    todos los vertices adjacentes, calcula la distancia 
    desde el vertice seleccionado anteriormente, si el vertice está
    en el conjunto se descarta, de lo contrario se elige otro vertice
    más cercano al elegido en el ciclo anterior */
    int min = INF;
    x = 0;
    y = 0;
    for (int i = 0; i < V; i++) {
      if (selected[i]) {
        for (int j = 0; j < V; j++) {
          /* Es verdadero cuando el vertice no estaba en el conjunto y es un lado */
          if (!selected[j] && G[i][j]) { 
            if (min >= G[i][j]) {
              min = G[i][j];
              x = i;
              y = j;
            }
          }
        }
      }
    }
    /* Se ignora la diagonal y se generan las salidas para el conjunto L */
    if(x != y){
      /* Se guardan en vectores los datos obtenidos del algoritmo
      para pasarlos al .txt que para graficarlo */
        pesos[no_edge] = G[x][y];
        nodo_ini[no_edge] = x;
        nodo_sig[no_edge] = y;
        cout << "(" << nodos[x] << ", " << nodos[y] << ")  :   "<< G[x][y] << "   :";
    }
    /* Se aumenta en 1 el contador de vertices */
    selected[y] = true;
    no_edge++;
    /* Se imprime el conjunto L */
    for(int i = 0; i < no_edge; i++){
      cout << "  (" << nodos[nodo_ini[i]] << ", " << nodos[nodo_sig[i]] << ")";
    }
        cout << endl;
  }
  /* Se envían los datos a graficar  */
  imprimir_grafo_prim(V, pesos, nodos, nodo_ini, nodo_sig);
}

/* Metodo que genera un grafo con los datos generados con el método "aplicar_prim" */
void Prim::imprimir_grafo_prim(int V, int *G, string *vector, int *A, int *B) {

    ofstream archivo("grafo.txt", ios::out);
    archivo << "graph G {" << endl;
    archivo << "graph [rankdir=LR]" << endl;
    archivo << "node [style=filled fillcolor=purple]" << endl << endl;
    /* Recorrerá los vectores e ingresará datos al archivo grafo.txt, de manera de formatear
    el grafo de la manera que buscamos */
    for(int i = 0; i < V - 1; i++){
        archivo << vector[A[i]] << " -- " << vector[B[i]] << " ";
        archivo << "[label=" + to_string(G[i]) << "];" << endl;
  }
      archivo << "}";
      /* Finalmente cierra el archivo generado */
      archivo.close();

    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}

/* Metodo que genera un grafo inicial" */
void Prim::imprimir_grafo(int V, int **G, string *vector) {

    ofstream archivo("grafo1.txt", ios::out);
    archivo << "graph G {" << endl;
    archivo << "graph [rankdir=LR]" << endl;
    archivo << "node [style=filled fillcolor=purple]" << endl << endl;
    /* Recorrerá la matriz e ingresará datos al archivo grafo.txt, de manera de formatear
    el grafo de la manera que buscamos */
    for(int i = 0; i < V ; i++){
      for(int j = 0; j < V; j++){
        if(i != j && G[i][j] > 0 && j > i){
          archivo << vector[i] << " -- " << vector[j] << " ";
          archivo << "[label=" + to_string(G[i][j]) << "];" << endl;
        }
    }
  }
      archivo << "}";
      /* Finalmente cierra el archivo generado */
      archivo.close();
    /* Se utiliza system para que al ejecutar el programa se abra el archivo .png */
    system("dot -Tpng -ografo1.png grafo1.txt");
    system("eog grafo1.png &");
}
