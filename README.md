# Guía VII UII

## Descripción del programa 
El programa permite la generación de un grafo dado una matriz de distancias entre nodos, más de dos nodos minimo, con estos datos son tratados con el algoritmo de Prim, el cual es un algoritmo perteneciente a la teoría de los grafos para encontrar un árbol recubridor mínimo en un grafo conexo, no dirigido y cuyas aristas están etiquetadas. En otras palabras, el algoritmo encuentra un subconjunto de aristas que forman un árbol con todos los vértices, donde el peso total de todas las aristas en el árbol es el mínimo posible. Si el grafo no es conexo, entonces el algoritmo encontrará el árbol recubridor mínimo para uno de los componentes conexos que forman dicho grafo no conexo. entregando finalmente dos imagenes llamada "grafo.png" y "grafo1.png" que contendrá tanto el grafo sin aplicar el algoritmo y el trantado con el algoritmo.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
* Software Graphviz
En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.
En el caso de no contar con Graphviz

### ¿Como instalar graphviz?
Para instalar graphviz, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install graphviz
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el paquete. Una vez termine la descarga, el paquete estará listo para usar

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa #Número entero#
```
* Es de vital importancia que se agrege un numero entero desde la consola y que este sea mayor a 2 como en el siguiente ejemplo.
```
./programa 5
```
* luego pedirá la distancia entre los nodos uno a uno, para finalmente generar una salida de la siguiente forma

## Paso 1
* Luego de ejecutar el programa este pedirá el nombre de los nodos, tantos como el usuario ingresó al ejecutar.
```
Ingrese el nombre del nodo 1: 1
Ingrese el nombre del nodo 2: 2
Ingrese el nombre del nodo 3: 3
Ingrese el nombre del nodo 4: 4
Ingrese el nombre del nodo 5: 5
```
## Paso 2
* Se pedirá la distancia entre los nodos de la siguiente manera.
```
Distancias del nodo: 1
Ingrese la distancia al nodo 2: 1
Ingrese la distancia al nodo 3: 3
Ingrese la distancia al nodo 4: 0
Ingrese la distancia al nodo 5: 0
Distancias del nodo: 2
Ingrese la distancia al nodo 3: 3
Ingrese la distancia al nodo 4: 6
Ingrese la distancia al nodo 5: 0
Distancias del nodo: 3
Ingrese la distancia al nodo 4: 4
Ingrese la distancia al nodo 5: 2
Distancias del nodo: 4
Ingrese la distancia al nodo 5: 5
```

## Salidas

* El programa entregará el contenido del conjunto L, ademas de los pesos.
```
(u, v)  : Costo :  L = {}
(1, 2)  :   1   :  (1, 2)
(2, 3)  :   3   :  (1, 2)  (2, 3)
(3, 5)  :   2   :  (1, 2)  (2, 3)  (3, 5)
(3, 4)  :   4   :  (1, 2)  (2, 3)  (3, 5)  (3, 4)
```

## PNG de salida, grafo inicial
![Caption for the picture.](https://gitlab.com/Dante1991HD/guia7/-/raw/main/grafo1.png)

## PNG de salida, grafo final
![Caption for the picture.](https://gitlab.com/Dante1991HD/guia7/-/raw/main/grafo.png)

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
