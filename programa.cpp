#include <cstring>
#include <iostream>
#include <fstream>
#include "Prim.h"

using namespace std;
/* Asigna valores a al vector de nodos, pide los valores al usuario */
void ini_nodos(int n, string *nodos) {
    string nombre;
    cout << endl;
    for(int i=0; i<n; i++){
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nodos[i] = nombre;
    }
}

/* Asigna valores a la matriz de dimensiones designadas por el usuario */
void ini_matriz(int n, string *nodos, int **matriz){ 
    int distancia;
    /* Llenar la matriz */
    for (int i=0; i<n; i++){
      /* Se utilizan los nodos creados para facilitar la lectura del usuario*/
        cout << "Distancias del nodo: " << nodos[i];
        cout << endl;
        for (int j=0; j<n; j++){
            if(i != j && j > i){
                cout << "Ingrese la distancia al nodo " << nodos[j] << ": ";
                cin >> distancia;
                matriz[i][j] = distancia;
            }
            else{
                /* Diagonal de la matriz*/
                matriz[i][j] = 0;
            }
        }
    }
}
// int ej1[6][6] = {
// {0, 6, 1, 5, 0, 0},
// {6, 0, 5, 0, 3, 0},
// {1, 5, 0, 5, 6, 4},
// {5, 0, 5, 0, 0, 2},
// {0, 3, 6, 0, 0, 6},
// {0, 0, 4, 2, 6, 0}}; 

// int ej2[5][5] = {
// {0, 1, 3, 0, 0},
// {1, 0, 3, 6, 0},
// {3, 3, 0, 4, 2},
// {0, 6, 4, 0, 5},
// {0, 2, 0, 5, 0}}; 

/* Función principal del programa */
int main(int argc, char **argv) {
  /* Variable "n" contiene el número de nodos inresados por consola */
  int n;
  /* Se convierte el string de la consola a entero */
  n = atoi(argv[1]);
  /* Variable "nodos" contendrá el nombre del nodo */
  string nodos[n];
  /* Variable "matriz" contiene las distancias entre los nodos
   el algoritmo de Prim utiliza una matriz simetrica, 
   por lo que solo se requieren los valores sobre o bajo la diagonal*/
  int **matriz;
  /* Se crea la matriz*/
  matriz = new int*[n];
  for(int i=0; i<n; i++){
      matriz[i] = new int[n];
  }
  /* Se instancia el objeto que contiene el algoritmo de Prim*/
  Prim prim;
  /* Validador de la cantidad minima de nodos, deben ser mayores a 2 */
  if (n <= 2) {
      cout << "Uso: \n./programa n\n con n > 2" << endl;
      return -1;
  }
  /* Se llama a la función que llena el vector de nodos */
  ini_nodos(n, nodos);
  /* Se llama a la función que llena la matriz */
  ini_matriz(n, nodos, matriz);
  /* Se llama al metodo que aplica el algoritmo de Prim */
  prim.aplicar_prim(n, matriz, nodos);
  /* Se llama al metodo que imprime los grafos */
  prim.imprimir_grafo(n, matriz, nodos);

  delete(matriz);

  return 0;
}