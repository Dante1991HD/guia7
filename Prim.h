#include <iostream>
#include <fstream>
#ifndef PRIM_H
#define PRIM_H

using namespace std;

class Prim {
    private:
        
    public:
        /* Construcntor por defecto */
        Prim();

        /* Metodo que aplica el algoritmo de Prim */
        void aplicar_prim(int V, int **matriz, string *etiqueta);
        /* Creacion e impresion del grafo para el algoritmo de prim (.png) */
        void imprimir_grafo_prim(int V, int *matriz, string *vector, int *A, int *B);
        /* Creacion e impresion del grafo inicial (.png) */
        void imprimir_grafo(int V, int **matriz, string *vector);

};
#endif